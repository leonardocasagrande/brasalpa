$("#hamburger-1").on("click", function () {
  $(this).toggleClass("menu-ativo");
  $(".top-nav").toggleClass("ativo");
  if ($(".top-nav").hasClass("ativo")) {
    $(".menu-btn")[0].checked = true;
  } else {
    $(".menu-btn")[0].checked = false;
  }
});

$(".menu-link").on("click", function (e) {
  e.preventDefault();
  $("html,body").animate(
    {
      scrollTop: eval($("#" + $(this).attr("target")).offset().top - 80),
    },
    100
  );
});

$(".top-nav .menu a").on("click", function (e) {
  $("#hamburger-1").toggleClass("menu-ativo");
  $(".top-nav").toggleClass("ativo");
});

$("#myTab2 a").on("click", function (event) {
  event.preventDefault();
  $("#myTab2 a").html("+");
  $(this).html('<i class="fas fa-times"></i>');
});

$("#exampleModal").on("shown.bs.modal", function () {
  window.history.pushState("object or string", "Title", "/assistir");
});

$("#exampleModal2").on("shown.bs.modal", function () {
  window.history.pushState("object or string", "Title", "/ouvir");
});

$("#exampleModal").on("hidden.bs.modal", function (e) {
  $("#exampleModal iframe").attr("src", $("#exampleModal iframe").attr("src"));
});

$("#exampleModal2").on("hidden.bs.modal", function (e) {
  $("#exampleModal2 iframe").attr(
    "src",
    $("#exampleModal2 iframe").attr("src")
  );
});

// $(' .close').on('click', function(){
//   console.log('teste');
//   $('.modal-body').each(function(){
//     $(this).stopVideo();
//   });
// });

$(function () {
  var sliderVorF = tns({
    container: ".carousel-v-f",
    items: 1,
    slideBy: 1,
    autoplayButtonOutput: false,
    nav: false,
    controls: true,
    controlsText: [
      "<i class='fas fa-chevron-left'></i>",
      "<i class='fas fa-chevron-right'></i>",
    ],
    loop: false,
  });
});

$(function () {
  var sliderBlog = tns({
    container: ".carousel-blog",
    items: 1,
    slideBy: 1,
    autoplayButtonOutput: false,
    nav: false,
    controls: true,
    controlsText: [
      "<i class='fas fa-chevron-left'></i>",
      "<i class='fas fa-chevron-right'></i>",
    ],
    nav: true,
    navPosition: "bottom",
    loop: false,
  });
});

$(document).on("scroll", function () {
  if ($(document).scrollTop() >= 40) {
    $(".menu-lg").addClass("black-bg");
  } else {
    $(".menu-lg").removeClass("black-bg");
  }
});

$(".e-mail").attr("onblur", "validateEmail(this)");

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$(".name input").on("input", function () {
  if (/[0-9]/g.test(this.value)) {
    alert("Apenas letras");
    this.value = "";
  }
});
