<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <meta name="adopt-website-id" content="a0571dff-acc7-4186-bc35-efb1396edd2a" />
  <script src="//tag.goadopt.io/injector.js?website_code=a0571dff-acc7-4186-bc35-efb1396edd2a" class="adopt-injector"></script>

  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-KKPC923');
  </script>
  <!-- End Google Tag Manager -->




  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    Movimento Repense

  </title>





  <meta name="facebook-domain-verification" content="4z3o9r9vl21yxcyq88a816yz6iw3ht" />

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">


</head>

<header class="">




  <div class="menu-lg d-none d-lg-flex">

    <a class="btn-home " href="<? get_site_url(); ?>/">
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">
    </a>

    <a class="menu-link" target="o-movimento">O MOVIMENTO</a>
    <a class="menu-link" target="o-plastico">O PLÁSTICO</a>
    <a class="menu-link" target="aprenda">APRENDA</a>

    <div class="midias">
      <a href="https://www.facebook.com/Movimento-Repense-100430912339234" target="_blank">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.png" alt="">
      </a>

      <a href="https://www.instagram.com/movimento.repense/" target="_blank">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.png" alt="">
      </a>


      <a href="https://www.youtube.com/channel/UCcgLE_HSeaJ2NviMfMtcglg" target="_blank">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yt.png" alt="">
      </a>

      <a class="no-filter" href="https://www.linkedin.com/company/movimento-repense/about/?viewAsMember=true" target="_blank">
        <i class="fab fa-linkedin-in"></i>
      </a>
    </div>
  </div>

  <?php if (!is_front_page()) : $menu_single = 'menu-single';
    $d_block = 'd-block';
  endif; ?>



  <nav class=" top-nav <?= $menu_single; ?>   d-lg-none " id="top-nav">


    <div class="hamburger" id="hamburger-1">
      <span class="line"></span>
      <span class="line"></span>
      <span class="line"></span>
    </div>

    <img class="only-single d-none <?= $d_block; ?> " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">



    <div class="menu">


      <a href="<?= get_site_url(); ?>">
        <img class="logo" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="BRASALPA">
      </a>

      <a href="#o-movimento">O MOVIMENTO</a>
      <a href="#o-plastico">O PLÁSTICO</a>
      <a href="#aprenda">APRENDA</a>


      <div class="share">

        <h3>COMPARTILHE</h3>

        <div class="col-6  px-0">

          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FMovimento-Repense-100430912339234&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.png" alt=""></a>



          <a href="https://www.instagram.com/movimento.repense/" target="_blank">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.png" alt="">
          </a>


          <a href="https://www.linkedin.com/company/movimento-repense/about/?viewAsMember=true" target="_blank">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </div>

      </div>
    </div>

  </nav>
</header>


<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KKPC923" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v11.0" nonce="WlEylHCq"></script>