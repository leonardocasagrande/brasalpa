<?php get_header(); ?>

<section class="banner">



  <!-- <video autoplay muted loop id="myVideo" class="d-none d-lg-block">
    <source src="<?= get_stylesheet_directory_uri(); ?>/dist/img/video.mp4" type="video/mp4">
  </video> -->

  <div class="container">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png  " alt="brasalpa">


    <h2>quem é o <span>vilão?</span></h2>



    <button class="btn-cta" type="button" data-toggle="modal" data-target="#exampleModal">assistir</button>

    <p class="d-none d-lg-block">role o mouse e conheça o movimento repense</p>

    <div class="lg-detail d-none d-lg-block"></div>

  </div>
</section>

<section id="o-movimento" class="o-movimento">


  <img class="d-none d-lg-block pack1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pack1.png" alt="">
  <img class="d-none d-lg-block pack2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pack2.png" alt="">

  <div class="infos container d-lg-flex">

    <div class="col-lg-6 px-0">
      <h3 class="title">o plástico não é uma epidemia.</h3>

      <h3 class="title">a falta de informação sim!</h3>
    </div>

    <div class="col-lg-5 px-0 text-lg">
      <p>O Movimento RePEnse nasce para fazer justiça ao plástico. Para ensinar que o que precisa mudar é a nossa relação com ele. Afinal, o plástico não tem culpa por ser descartado de maneira errada. </p>

      <p>Nós fizemos as escolhas que nos trouxeram até aqui e é nossa responsabilidade trabalhar para solucionar o problema.</p>

      <p><b>O plástico não é o vilão.</br>
          RePEnse. Vem com a gente.</b></p>
    </div>

    <div class="d-lg-none">
      <h3 class="title mb-0 mt-5">se liga!</h3>
      <h3 class="title mb-0">o que é isso?</h3>

      <h4 class="black-box title">o plástico não é vilão, mas ninguém te disse isso</h4>
    </div>

  </div>

  <div class="position-relative d-lg-flex ">

    <div class="d-lg-block d-none col-lg-5 pl-0 pr-5">
      <h3 class="title mb-0 mt-5">se liga!</h3>
      <h3 class="title mb-0">o que é isso?</h3>

      <h4 class="black-box title">o plástico não é vilão, mas ninguém te disse isso</h4>
    </div>

    <img class="d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/woman-bg.png" alt="">


    <div class="player">

      <button class="play-box " type="button" data-toggle="modal" data-target="#exampleModal2"><i class="fas fa-play"></i></button>


      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/timeline.png" alt="">

      <h4 class="music-name">repare, repense</br><span>mc pet</span></h4>
    </div>

    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FMovimento-Repense-100430912339234&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore share"> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/share.png" alt=""></a>

  </div>

  <img class="d-lg-block d-none img-lg" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/woman-bg-lg.png" alt="">


</section>

<section id="o-plastico" class="o-plastico">

  <img class="d-none d-lg-block pack3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pack3.png" alt="">
  <img class="d-none d-lg-block pack4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pack4.png" alt="">


  <div class="lg-wrapper">
    <h3 class="title">mitos e verdades</h3>

    <div class="wrapper-carousel">

      <div class="carousel-v-f">


        <div class="item">
          <div class="question">

            <h4 class="sub">todos os materiais plásticos são recicláveis</h4>


            <ul class="nav nav-tabs d-lg-flex col-lg-4 px-0" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link " id="mito-tab" data-toggle="tab" href="#mito1" role="tab" aria-controls="home" aria-selected="false">mito</a>
              </li>

              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link" id="verdade-tab" data-toggle="tab" href="#verdade1" role="tab" aria-controls="profile" aria-selected="false">verdade</a>
              </li>

            </ul>

          </div>


          <div class="tab-content answer">
            <div class="tab-pane " id="mito1" role="tabpanel" aria-labelledby="mito-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">01</h3>

                <div>
                  <h4 class="v-or-f">Acertou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Alguns tipos de plástico, como embalagens metalizadas, adesivos e papel celofane não são recicláveis e, por isso, não podem ser descartadas na coleta seletiva.</p>

                  <a href="https://www.bbc.com/portuguese/geral-50564869#:~:text=O%20Brasil%20est%C3%A1%20atr%C3%A1s%20dos,ir%20para%20a%20coleta%20seletiva." target="_blank">(fonte: Maioria dos brasileiros não sabe como funciona a reciclagem)</a>
                </div>
              </div>

            </div>

            <div class="tab-pane" id="verdade1" role="tabpanel" aria-labelledby="verdade-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">01</h3>

                <div>
                  <h4 class="v-or-f">errou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Alguns tipos de plástico, como embalagens metalizadas, adesivos e papel celofane não são recicláveis e, por isso, não podem ser descartadas na coleta seletiva.</p>

                  <a href="https://www.bbc.com/portuguese/geral-50564869#:~:text=O%20Brasil%20est%C3%A1%20atr%C3%A1s%20dos,ir%20para%20a%20coleta%20seletiva." target="_blank">(fonte: Maioria dos brasileiros não sabe como funciona a reciclagem)</a>
                </div>
              </div>

            </div>

          </div>

        </div>

        <div class="item">
          <div class="question">

            <h4 class="sub">80% do lixo encontrado nos oceanos é plástico</h4>


            <ul class="nav nav-tabs d-lg-flex col-lg-4 px-0" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link " id="mito-tab" data-toggle="tab" href="#mito2" role="tab" aria-controls="home" aria-selected="false">mito</a>
              </li>

              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link" id="verdade-tab" data-toggle="tab" href="#verdade2" role="tab" aria-controls="profile" aria-selected="false">verdade</a>
              </li>

            </ul>

          </div>


          <div class="tab-content answer">
            <div class="tab-pane " id="mito2" role="tabpanel" aria-labelledby="mito-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">02</h3>

                <div>
                  <h4 class="v-or-f">Errou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Este número assustador é composto majoritariamente por sacolas, garrafas e recipientes descartáveis.</p>

                  <a href="https://www.dw.com/pt-br/pl%C3%A1stico-%C3%A9-respons%C3%A1vel-por-80-do-lixo-nos-oceanos/a-57859624" target="_blank">(fonte: Plástico é responsável por 80% do lixo nos oceanos | Notícias internacionais e análises | DW | 11.06.2021)</a>
                </div>
              </div>

            </div>

            <div class="tab-pane" id="verdade2" role="tabpanel" aria-labelledby="verdade-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">02</h3>

                <div>
                  <h4 class="v-or-f">acertou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Este número assustador é composto majoritariamente por sacolas, garrafas e recipientes descartáveis.</p>

                  <a href="https://www.dw.com/pt-br/pl%C3%A1stico-%C3%A9-respons%C3%A1vel-por-80-do-lixo-nos-oceanos/a-57859624" target="_blank">(fonte: Plástico é responsável por 80% do lixo nos oceanos | Notícias internacionais e análises | DW | 11.06.2021)</a>
                </div>
              </div>

            </div>

          </div>

        </div>

        <div class="item">
          <div class="question">

            <h4 class="sub">A maioria dos brasileiros conhece as regras de reciclagem</h4>


            <ul class="nav nav-tabs d-lg-flex col-lg-4 px-0" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link " id="mito-tab" data-toggle="tab" href="#mito3" role="tab" aria-controls="home" aria-selected="false">mito</a>
              </li>

              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link" id="verdade-tab" data-toggle="tab" href="#verdade3" role="tab" aria-controls="profile" aria-selected="false">verdade</a>
              </li>

            </ul>

          </div>


          <div class="tab-content answer">
            <div class="tab-pane " id="mito3" role="tabpanel" aria-labelledby="mito-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">03</h3>

                <div>
                  <h4 class="v-or-f">acertou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Para citar um exemplo, 65% da população brasileira acredita que todos os plásticos são recicláveis.</p>

                  <a href='https://www.bbc.com/portuguese/geral-50564869' target="_blank">(fonte: Maioria dos brasileiros não sabe como funciona a reciclagem, diz pesquisa - BBC News Brasil)</a>
                </div>
              </div>

            </div>

            <div class="tab-pane" id="verdade3" role="tabpanel" aria-labelledby="verdade-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">03</h3>

                <div>
                  <h4 class="v-or-f">errou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Para citar um exemplo, 65% da população brasileira acredita que todos os plásticos são recicláveis.</p>

                  <a href='https://www.bbc.com/portuguese/geral-50564869' target="_blank">(fonte: Maioria dos brasileiros não sabe como funciona a reciclagem, diz pesquisa - BBC News Brasil)</a>
                </div>
              </div>

            </div>

          </div>

        </div>

        <div class="item">
          <div class="question">

            <h4 class="sub">Embalagens plásticas podem contaminar alimentos</h4>


            <ul class="nav nav-tabs d-lg-flex col-lg-4 px-0" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link " id="mito-tab" data-toggle="tab" href="#mito4" role="tab" aria-controls="home" aria-selected="false">mito</a>
              </li>

              <li class="nav-item" role="presentation">
                <a class="btn-cta nav-link" id="verdade-tab" data-toggle="tab" href="#verdade4" role="tab" aria-controls="profile" aria-selected="false">verdade</a>
              </li>

            </ul>

          </div>


          <div class="tab-content answer">
            <div class="tab-pane " id="mito4" role="tabpanel" aria-labelledby="mito-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">04</h3>

                <div>
                  <h4 class="v-or-f">acertou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Embalagens plásticas são feitas para resistir, inclusive, a variações de temperatura sem liberar substâncias de sua composição. É justamente pelos benefícios de segurança à saúde, além da leveza e maleabilidade, que o plástico se torna a melhor e mais barata opção. </p>

                  <a href='https://www.astra-sa.com/destaques/mitos-e-verdades-sobre-o-plastico/' target="_blank">(fonte: Mitos e verdades sobre o plástico - Blog Astra (astra-sa.com))</a>
                </div>
              </div>

            </div>

            <div class="tab-pane" id="verdade4" role="tabpanel" aria-labelledby="verdade-tab">

              <div class="d-lg-flex align-items-center">
                <h3 class="number">04</h3>

                <div>
                  <h4 class="v-or-f">errou</h4>

                  <p class="col-lg-10 px-0 pr-lg-4">Embalagens plásticas são feitas para resistir, inclusive, a variações de temperatura sem liberar substâncias de sua composição. É justamente pelos benefícios de segurança à saúde, além da leveza e maleabilidade, que o plástico se torna a melhor e mais barata opção. </p>

                  <a href='https://www.astra-sa.com/destaques/mitos-e-verdades-sobre-o-plastico/' target="_blank">(fonte: Mitos e verdades sobre o plástico - Blog Astra (astra-sa.com))</a>
                </div>
              </div>

            </div>

          </div>

        </div>


      </div>
    </div>

    <div class="share d-none d-lg-flex col-lg-3 mt-lg-5">

      <h3>COMPARTILHE</h3>

      <div class="col-6 col-lg-5 px-0">
        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FMovimento-Repense-100430912339234&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.png" alt=""></a>

        <a href="https://www.instagram.com/movimento.repense/" target="_blank">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.png" alt="">
        </a>


        <a href="https://www.linkedin.com/company/movimento-repense/about/?viewAsMember=true" target="_blank">
          <i class="fab fa-linkedin-in"></i>
        </a>
      </div>

    </div>

  </div>
</section>

<section id="aprenda" class="aprenda">

  <div class="container">

    <div class="infos d-lg-flex align-items-flex-start">
      <h3 class="title d-lg-none">5 fatos sobre o plástico que ninguém te contou!</h3>

      <div class="title d-none d-lg-block col-lg-2 px-0">mire no conhecimento e <span>repense o seu vilão</span></div>

      <div class="bg-lg-map col-lg-9 px-0">
        <div class="bottle-bg mt-lg-0">

          <img class="bottle" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bottle.png" alt="">


          <ul class="nav nav-tabs" id="myTab2" role="tablist">
            <li class="" role="presentation">
              <a class="nav-link btn-plus b1 active" id="home-tab" data-toggle="tab" href="#b1" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-times"></i></a>
            </li>
            <li class="" role="presentation">
              <a class="nav-link btn-plus b2" id="profile-tab" data-toggle="tab" href="#b2" role="tab" aria-controls="profile" aria-selected="false">+</a>
            </li>
            <li class="" role="presentation">
              <a class="nav-link btn-plus b3" id="messages-tab" data-toggle="tab" href="#b3" role="tab" aria-controls="messages" aria-selected="false">+</a>
            </li>
            <li class="" role="presentation">
              <a class="nav-link btn-plus b4" id="settings-tab" data-toggle="tab" href="#b4" role="tab" aria-controls="settings" aria-selected="false">+</a>
            </li>
            <li class="" role="presentation">
              <a class="nav-link btn-plus b5" id="last-tab" data-toggle="tab" href="#b5" role="tab" aria-controls="settings" aria-selected="false">+</a>
            </li>
          </ul>




        </div>
      </div>

      <div class="black-box ">

        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="b1" role="tabpanel" aria-labelledby="home-tab">
            <p><b>No caminho certo</b></p>
            <p>Ainda há muito a ser feito, mas a reciclagem mecânica de plásticos está crescendo no Brasil e registrou seu recorde histórico no ano de 2019: 24% do material pós-consumo foi reciclado. Para se ter uma ideia, em 2018, o índice foi de 22,1%.
            </p>
            <a href="https://agenciabrasil.ebc.com.br/geral/noticia/2021-02/indice-de-reciclagem-de-plastico-no-pais-cresceu-85-em-2019" target="_blank">Fonte: Índice de reciclagem de plástico no país cresceu 8,5% em 2019 | Agência Brasil (ebc.com.br)</a>
          </div>

          <div class="tab-pane" id="b2" role="tabpanel" aria-labelledby="profile-tab">

            <p><b>Se a moda pega</b></p>
            <p>Grandes empresas estão eliminando o plástico dos seus produtos e serviços e outras estão fazendo o plástico circular, utilizando material reciclado na produção de novas mercadorias, inclusive, marcas de artigos esportivos e grifes de moda.
            </p>
            <a href="https://epocanegocios.globo.com/Empresa/noticia/2019/03/estas-empresas-estao-reduzindo-o-uso-de-plastico-em-seus-produtos.html" target="_blank">Fonte: Estas empresas estão reduzindo o uso de plástico em seus produtos - Época Negócios | Empresa (globo.com)</a>

            <a href="https://exame.com/bussola/iogurteria-nestle-tera-todas-as-embalagens-feitas-de-plastico-reciclado/" target="_blank">Fonte: Iogurteria Nestlé terá todas as embalagens feitas de plástico reciclado</a>

          </div>

          <div class="tab-pane" id="b3" role="tabpanel" aria-labelledby="messages-tab">

            <p><b>Dá trabalho e gera emprego</b></p>
            <p>Desenvolvimento sustentável não só é possível, como também é rentável. Um campo de oportunidades de empreendedorismo, inovação e geração de empregos. Só em 2019, o faturamento bruto chegou a R$ 2.5 bilhões.
            </p>
            <a href="http://www.plasticotransforma.com.br/estudo-sobre-a-industria-de-reciclagem-no-brasil" target="_blank">Fonte: Muito além dos números da Indústria de Reciclagem de Plástico (plasticotransforma.com.br)</a>


          </div>

          <div class="tab-pane" id="b4" role="tabpanel" aria-labelledby="settings-tab">
            <p><b>Se organizar direitinho…</b></p>
            <p>Muita gente não conseguiu esperar a situação se resolver sozinha e está agindo para contribuir para a solução. É o caso de muitos projetos sem fins econômicos, bem como de empresas que compreendem a importância da causa. Se organizar direitinho, todo mundo ajuda. Conheça as iniciativas:
            </p>
            <a href="https://greenminds.com.br/blogs/news/reciclagem-de-plastico-no-brasil" target="_blank">Reciclagem de plástico no Brasil: 8 projetos que promovem limpezas – Green Minds</a>
          </div>

          <div class="tab-pane" id="b5" role="tabpanel" aria-labelledby="last-tab">
            <p><b>Astro de cinema</b></p>
            <p>O plástico ficou famoso pelos motivos errados e se tornou tema de diversos documentários. Com a ascensão da internet e das plataformas de streaming, este tipo de conteúdo está cada vez mais acessível.
            </p>

          </div>

        </div>



      </div>
    </div>


  </div>
</section>



<section class="diario-do-plastico">

  <img class="d-none d-lg-block pack5" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pack5.png" alt="">


  <div class="wrapper-lg">
    <div class="header">
      <h3 class="title">diário do plástico</h3>
    </div>

    <div class="wrapper-carousel">

      <?php
      $argsPosts = array(
        'post_type' => 'post',
        'order' => 'DESC',
        'posts_per_page' => -1,


      );
      $posts = new WP_Query($argsPosts);


      if ($posts->have_posts()) :; ?>

        <div class="carousel-blog">

          <?php while ($posts->have_posts()) :  $posts->the_post(); ?>


            <div class="item">

              <div class="wrapper d-md-flex">

                <div class="img" style="background: url(<?= get_the_post_thumbnail_url(); ?>) 72% 50%;"></div>
                <div class="black">
                  <h3 class="title"><?= the_title(); ?></h3>

                  <a href="<?= the_permalink(); ?>" class="read">+ler mais</a>
                </div>

              </div>

            </div>

          <?php endwhile; ?>


        </div>

      <?php else : ?>

        <p>Não encontramos nenhum post.</p>

      <?php endif; ?>

      <div class="share col-lg-3 d-none d-lg-flex">

        <h3>COMPARTILHE</h3>

        <div class="col-6 col-lg-4 px-0">
          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FMovimento-Repense-100430912339234&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.png" alt=""></a>

          <a href="https://www.instagram.com/movimento.repense/" target="_blank">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.png" alt="">
          </a>


          <a href="https://www.linkedin.com/company/movimento-repense/about/?viewAsMember=true" target="_blank">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </div>
      </div>
    </div>



    <div class="yellow-box">

      <h4 class="title ">APRENDA COM O PLÁSTICO, faça ele circular E MUDE O FUTURO!</h4>

      <div class="wrapper">
        <img class="menino" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/menino.png" alt="menino">

        <div class="pdfs-box d-lg-flex  align-items-baseline">
        <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/ebook3-como-o-plastico-revolucionou-a-historia-da-sociedade.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Como o plástico revolucionou a história da sociedade?</span></h4>
            </div>
          </a>

          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/ebook-o-plastico-e-o-vilao.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>O plástico é o vilão?</span></h4>
            </div>
          </a>



          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/ebook-o-que-e-economia-circular.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>O que é economia circular?</span></h4>

            </div>
          </a>


          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/e-book-Prazer-eu-sou-o-plástico.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Prazer, eu sou o plástico</span></h4>

            </div>
          </a>

          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/Como_ensinar_reciclagem_para_as_criancas.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Como ensinar reciclagem para as crianças?</span></h4>

            </div>
          </a>

          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/The-Plastics-Paradox.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">Livro: <span>O paradoxo dos plásticos - Fatos para um futuro melhor</span></h4>

            </div>
          </a>

          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/ebook-10-perguntas-e-respostas.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>10 Questões sobre reciclagem</span></h4>

            </div>
          </a>

          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/e-book-beneficios-do-plastico-para-o-meio-ambiente.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Benefícios do plástico para o meio ambiente</span></h4>

            </div>
          </a>

          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/Atitudes_para_nao_culpar_o_plastico.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Atitudes para não culpar o plástico</span></h4>

            </div>
          </a>
          <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/12234_e-book_plasticos-alternativos.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Plásticos alternativos no mercado</span></h4>

            </div>
          </a>
          <!-- JANEIRO <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/12234_e-book_praticas_que_facilitam_a_vida_dos_catadores.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>Práticas que facilitam a vida dos catadores</span></h4>

            </div>
          </a> -->
          <!-- FEVEREIRO <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/12234-E-book-3_3_Fevereiro-Os-10-Rs-da-sustentabilidade-do-plastico.pdf" target="_blank" class="wrapper-btn col-lg-6 d-lg-flex">
            <div class="item">

              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pdf-icon.png" alt="">
              <h4 class="name">E-book: <span>10 Rs da sustentabilidade do plástico</span></h4>

            </div>
          </a> -->
        </div>
      </div>
    </div>
  </div>
</section>

<section class="instagram">
  <div class="container">
    <h3 class="title col-lg-5 px-3">Quer ainda mais conteúdo? Segue a gente no Instagram</h3>
    <?php echo do_shortcode('[instagram-feed]'); ?>
  </div>
</section>

<section class="form-box">

  <img class="pack d-none d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pack.png" alt="" class="d-none d-lg-block">

  <div class="container">

    <h3 class="title col-lg-5 px-0">tem dúvida, sugestão ou quer saber mais sobre o plástico?</h3>

    <p class="sub col-lg-4 px-0">assine a nossa newsletter para receber conteúdos exclusivos.</p>

    <div class="form d-lg-flex  col-lg-10 px-0">

      <?= do_shortcode('[contact-form-7 id="11" title="Formulário de contato"]'); ?>

    </div>

  </div>


</section>




<?php get_footer(); ?>