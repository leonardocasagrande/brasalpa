<footer>
  <div class="container">

    <a href="<?= get_stylesheet_directory_uri(); ?>/dist/img/Politica-Privacidade-Repense.pdf" target="_blank" class="politica d-none d-lg-block">política de privacidade</a>

    <img class="col-6 col-lg-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="repense">

    <div class="d-lg-flex">
      <h4 class="foot  pr-0">2021</h4>
      <h4 class="foot  pr-0">todos os direitos reservados</h4>
    </div>

  </div>

  <?php if (is_front_page()) :; ?>
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/footer-detail.png" alt="" class="d-none d-lg-block footer-detail">
  <?php endif; ?>



</footer>



<!-- Modal Banner -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="wrapper">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-body">
          <iframe class="video" width="560" height="315" src="https://www.youtube.com/embed/i0qHK7OKuBE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal Jingle -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="wrapper">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-body">
          <iframe class="video" width="560" height="315" src="https://www.youtube.com/embed/xQoMwK7_LBI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

      </div>
    </div>
  </div>
</div>


<?php wp_footer(); ?>


<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>


<script>
  $('#myModal').on('shown.bs.modal', function() {
    $('#myInput').trigger('focus')
  })
</script>


</body>

</html>